package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private ProjectDTO project;

    @Nullable
    private List<ProjectDTO> projects;

    public AbstractProjectResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

    public AbstractProjectResponse(@Nullable final List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
