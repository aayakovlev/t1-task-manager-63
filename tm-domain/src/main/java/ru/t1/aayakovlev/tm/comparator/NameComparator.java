package ru.t1.aayakovlev.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.HasName;

import java.util.Comparator;

public enum NameComparator implements Comparator<HasName> {

    INSTANCE;

    @Override
    public int compare(@Nullable final HasName o1, @Nullable final HasName o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
