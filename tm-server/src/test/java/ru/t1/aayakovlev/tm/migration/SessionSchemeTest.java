package ru.t1.aayakovlev.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;

public final class SessionSchemeTest extends AbstractSchemeTest {

    @Test
    public void testSessionScheme() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
        liquibase.dropAll();
        liquibase.update("session");
    }

}
