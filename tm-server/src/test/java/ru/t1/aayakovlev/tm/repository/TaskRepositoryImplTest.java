//package ru.t1.aayakovlev.tm.repository;
//
//import liquibase.Liquibase;
//import liquibase.exception.LiquibaseException;
//import lombok.SneakyThrows;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.*;
//import org.junit.experimental.categories.Category;
//import ru.t1.aayakovlev.tm.MeasureHelper;
//import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
//import ru.t1.aayakovlev.tm.exception.AbstractException;
//import ru.t1.aayakovlev.tm.marker.UnitCategory;
//import ru.t1.aayakovlev.tm.migration.AbstractSchemeTest;
//import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;
//import ru.t1.aayakovlev.tm.repository.dto.impl.TaskDTORepositoryImpl;
//import ru.t1.aayakovlev.tm.service.ConnectionService;
//import ru.t1.aayakovlev.tm.service.PropertyService;
//import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;
//import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;
//import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
//import ru.t1.aayakovlev.tm.service.dto.impl.ProjectDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.dto.impl.TaskDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;
//import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
//import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
//import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;
//
//@Category(UnitCategory.class)
//public final class TaskRepositoryImplTest extends AbstractSchemeTest {
//
//    @NotNull
//    private static PropertyService propertyService;
//
//    @NotNull
//    private static ConnectionService connectionService;
//
//    @NotNull
//    private static EntityManager entityManager;
//
//    @Nullable
//    private static UserDTOService userService;
//
//    @Nullable
//    private static ProjectDTOService projectService;
//
//    @NotNull
//    private static TaskDTORepository repository;
//
//    @NotNull
//    private static TaskDTOService service;
//
//    @BeforeClass
//    public static void initConnectionService() throws LiquibaseException {
//        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
//        liquibase.dropAll();
//        liquibase.update("scheme");
//
//        propertyService = new PropertyServiceImpl();
//        connectionService = new ConnectionServiceImpl(propertyService);
//        entityManager = connectionService.getEntityManager();
//        repository = new TaskDTORepositoryImpl(entityManager);
//        service = new TaskDTOServiceImpl(connectionService);
//        userService = new UserDTOServiceImpl(connectionService, propertyService);
//        projectService = new ProjectDTOServiceImpl(connectionService);
//    }
//
//    @AfterClass
//    public static void destroyConnection() {
//        connectionService.close();
//    }
//
//    @Before
//    public void initData() throws AbstractException {
//        userService.save(COMMON_USER_ONE);
//        userService.save(ADMIN_USER_ONE);
//        projectService.save(PROJECT_ADMIN_ONE);
//        projectService.save(PROJECT_ADMIN_TWO);
//        projectService.save(PROJECT_USER_ONE);
//        projectService.save(PROJECT_USER_TWO);
//        service.save(TASK_USER_ONE);
//        service.save(TASK_USER_TWO);
//    }
//
//    @After
//    public void after() throws AbstractException {
//        service.deleteAll();
//        projectService.deleteAll();
//        userService.deleteAll();
//    }
//
//    @Test
//    public void When_FindByIdExistsTask_Expect_ReturnTask() {
//        @Nullable final TaskDTO task = repository.findByUserIdAndId(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
//        Assert.assertNotNull(task);
//        Assert.assertEquals(TASK_USER_ONE.getDescription(), task.getDescription());
//        Assert.assertEquals(TASK_USER_ONE.getName(), task.getName());
//        Assert.assertEquals(TASK_USER_ONE.getStatus(), task.getStatus());
//        Assert.assertEquals(TASK_USER_ONE.getUserId(), task.getUserId());
//        Assert.assertEquals(TASK_USER_ONE.getCreated(), task.getCreated());
//    }
//
//    @Test
//    public void When_FindByIdExistsTask_Expect_ReturnNull() {
//        @Nullable final TaskDTO task = repository.findByUserIdAndId(USER_ID_NOT_EXISTED, TASK_ID_NOT_EXISTED);
//        Assert.assertNull(task);
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_SaveNotNullTask_Expect_ReturnTask() {
//        MeasureHelper.measure(entityManager, () -> repository.save(TASK_ADMIN_ONE));
//        @Nullable final TaskDTO task = repository.findByUserIdAndId(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE.getId());
//        Assert.assertNull(task);
//    }
//
//    @Test
//    public void When_CountCommonUserTasks_Expect_ReturnTwo() {
//        final int countByUserId = repository.countByUserId(COMMON_USER_ONE.getId());
//        Assert.assertEquals(2, countByUserId);
//    }
//
//    @Test
//    public void When_FindAllUserId_Expected_ReturnListTasks() {
//        final List<TaskDTO> tasks = repository.findAllByUserId(COMMON_USER_ONE.getId());
//        for (int i = 0; i < tasks.size(); i++) {
//            Assert.assertEquals(USER_TASK_LIST.get(i).getName(), tasks.get(i).getName());
//        }
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_RemoveExistedTask_Expect_FindNull() {
//        MeasureHelper.measure(entityManager, () -> repository.save(TASK_ADMIN_TWO));
//        @Nullable final TaskDTO task = repository.findByUserIdAndId(TASK_USER_ONE.getId(), TASK_ADMIN_TWO.getId());
//        Assert.assertNull(task);
//
//        MeasureHelper.measure(entityManager, () -> repository.deleteByUserIdAndId(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId()));
//        @Nullable final TaskDTO taskRemoved = repository.findByUserIdAndId(TASK_USER_ONE.getId(), TASK_ADMIN_TWO.getId());
//        Assert.assertNull(taskRemoved);
//    }
//
//    @Test
//    public void When_RemoveNotTask_Expect_Nothing() {
//        MeasureHelper.measure(entityManager, () -> repository.deleteByUserIdAndId(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId()));
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_RemoveAll_Expect_ZeroCountTasks() {
//        MeasureHelper.measure(entityManager, () -> repository.save(TASK_ADMIN_ONE));
//        MeasureHelper.measure(entityManager, () -> repository.save(TASK_ADMIN_TWO));
//        MeasureHelper.measure(entityManager, () -> repository.deleteAll(ADMIN_USER_ONE.getId()));
//        Assert.assertEquals(0, repository.countByUserId(ADMIN_USER_ONE.getId()));
//    }
//
//    @Test
//    public void When_RemoveByIdNotExistedTask_Expect_ThrowsEntityNotFoundException() {
//        MeasureHelper.measure(entityManager, () -> repository.deleteByUserIdAndId(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId()));
//    }
//
//    @Test
//    public void When_FindAllByProjectIdExistedProject_Expect_ReturnTaskList() {
//        final List<TaskDTO> tasks = repository.findAllByUserIdAndProjectId(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
//        Assert.assertNotEquals(0, tasks.size());
//    }
//
//    @Test
//    public void When_FindAllByProjectIdNotExistedProject_Expect_ReturnEmptyList() {
//        final List<TaskDTO> tasks = repository.findAllByUserIdAndProjectId(COMMON_USER_ONE.getId(), PROJECT_ID_NOT_EXISTED);
//        Assert.assertEquals(0, tasks.size());
//    }
//
//}
