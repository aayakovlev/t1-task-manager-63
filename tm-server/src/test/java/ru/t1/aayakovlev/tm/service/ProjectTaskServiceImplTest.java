//package ru.t1.aayakovlev.tm.service;
//
//import liquibase.Liquibase;
//import liquibase.exception.LiquibaseException;
//import org.jetbrains.annotations.NotNull;
//import org.junit.*;
//import org.junit.experimental.categories.Category;
//import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
//import ru.t1.aayakovlev.tm.exception.AbstractException;
//import ru.t1.aayakovlev.tm.marker.UnitCategory;
//import ru.t1.aayakovlev.tm.migration.AbstractSchemeTest;
//import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;
//import ru.t1.aayakovlev.tm.service.dto.ProjectTaskDTOService;
//import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;
//import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
//import ru.t1.aayakovlev.tm.service.dto.impl.ProjectDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.dto.impl.ProjectTaskDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.dto.impl.TaskDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
//
//import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;
//import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
//import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
//import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;
//
//@Category(UnitCategory.class)
//public final class ProjectTaskServiceImplTest extends AbstractSchemeTest {
//
//    @NotNull
//    private static PropertyService propertyService;
//
//    @NotNull
//    private static ConnectionService connectionService;
//
//    @NotNull
//    private static ProjectDTOService projectService;
//
//    @NotNull
//    private static TaskDTOService taskService;
//
//    @NotNull
//    private static ProjectTaskDTOService service;
//
//    @NotNull
//    private static UserDTOService userService;
//
//    @BeforeClass
//    public static void initConnectionService() throws LiquibaseException {
//        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
//        liquibase.dropAll();
//        liquibase.update("scheme");
//
//        propertyService = new PropertyServiceImpl();
//        connectionService = new ConnectionServiceImpl(propertyService);
//        projectService = new ProjectDTOServiceImpl(connectionService);
//        taskService = new TaskDTOServiceImpl(connectionService);
//        service = new ProjectTaskDTOServiceImpl(connectionService);
//        userService = new UserDTOServiceImpl(connectionService, propertyService);
//    }
//
//    @AfterClass
//    public static void destroyConnection() {
//        connectionService.close();
//    }
//
//    @Before
//    public void initData() throws AbstractException {
//        userService.save(ADMIN_USER_ONE);
//        userService.save(COMMON_USER_ONE);
//        projectService.save(PROJECT_USER_ONE);
//        projectService.save(PROJECT_USER_TWO);
//        taskService.save(TASK_USER_ONE);
//        taskService.save(TASK_USER_TWO);
//    }
//
//    @After
//    public void after() throws AbstractException {
//        taskService.deleteAll();
//        projectService.deleteAll();
//        userService.deleteAll();
//    }
//
//    @Test
//    public void When_BindTaskToProject_Expect_TaskWithProjectId() throws AbstractException {
//        projectService.save(PROJECT_ADMIN_ONE);
//        taskService.save(TASK_EMPTY_PROJECT_ID);
//        @NotNull final TaskDTO task = service.bindTaskToProject(
//                ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId(), TASK_EMPTY_PROJECT_ID.getId()
//        );
//        Assert.assertNotNull(task.getProjectId());
//    }
//
//    @Test
//    public void When_UnbindTaskFromProject_Expect_TaskWithoutProjectId() throws AbstractException {
//        projectService.save(PROJECT_ADMIN_ONE);
//        taskService.save(TASK_ADMIN_ONE);
//        @NotNull final TaskDTO task = service.unbindTaskFromProject(
//                ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId(), TASK_ADMIN_ONE.getId()
//        );
//        Assert.assertNull(task.getProjectId());
//    }
//
//    @Test
//    public void When_RemoveProjectById_Expect_ThrowsEntityNotFoundException() throws AbstractException {
//        projectService.save(PROJECT_ADMIN_ONE);
//        projectService.save(PROJECT_ADMIN_TWO);
//        taskService.save(TASK_ADMIN_ONE);
//        taskService.save(TASK_ADMIN_TWO);
//        Assert.assertTrue(taskService.findAllByUserIdAndProjectId(ADMIN_USER_TWO.getId(), PROJECT_ADMIN_TWO.getId()).isEmpty());
//    }
//
//}
