package ru.t1.aayakovlev.tm.service.dto.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.TaskIdEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;
import ru.t1.aayakovlev.tm.service.dto.ProjectTaskDTOService;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskDTOServiceImpl implements ProjectTaskDTOService {

    @Getter
    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!getProjectRepository().findByUserIdAndId(userId, projectId).isPresent()) throw new ProjectNotFoundException();
        @NotNull final Optional<TaskDTO> resultTask = getTaskRepository().findByUserIdAndId(userId, taskId);
        if (!resultTask.isPresent()) throw new TaskNotFoundException();
        @NotNull final TaskDTO task = resultTask.get();
        task.setProjectId(projectId);
        task.setUserId(userId);
        getTaskRepository().save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!getProjectRepository().findByUserIdAndId(userId, projectId).isPresent()) throw new ProjectNotFoundException();
        @NotNull final Optional<TaskDTO> resultTask = getTaskRepository().findByUserIdAndId(userId, taskId);
        if (!resultTask.isPresent()) throw new TaskNotFoundException();
        @NotNull final TaskDTO task = resultTask.get();
        task.setProjectId(null);
        task.setUserId(userId);
        getTaskRepository().save(task);
        return task;
    }

    @Override
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<ProjectDTO> project = getProjectRepository().findByUserIdAndId(userId, projectId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        getTaskRepository().deleteAllByUserIdAndProjectId(userId, projectId);
        getProjectRepository().deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @NotNull final List<ProjectDTO> projects = getProjectRepository().findAllByUserId(userId);
        for (@NotNull final ProjectDTO project : projects) {
            getTaskRepository().deleteAllByUserIdAndProjectId(userId, project.getId());
        }
        getProjectRepository().deleteAllByUserId(userId);
    }

}
