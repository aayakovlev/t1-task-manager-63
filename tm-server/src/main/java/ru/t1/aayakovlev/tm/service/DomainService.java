package ru.t1.aayakovlev.tm.service;

import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.Domain;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface DomainService {

    @NotNull
    Domain getDomain() throws AbstractException;

    void setDomain(@Nullable final Domain domain) throws AbstractException;

    void backupLoad();

    void backupSave();

    void base64Load();

    void base64Save();

    void binaryLoad();

    void binarySave();

    void jsonLoadFXml();

    void jsonLoadJaxB();

    void jsonSaveFXml();

    void jsonSaveJaxB();

    void xmlLoadFXml();

    void xmlLoadJaxB();

    void xmlSaveFXml();

    void xmlSaveJaxB();

    void yamlLoad();

    void yamlSave();

    void initScheme() throws LiquibaseException;

    void dropScheme() throws DatabaseException;

}
