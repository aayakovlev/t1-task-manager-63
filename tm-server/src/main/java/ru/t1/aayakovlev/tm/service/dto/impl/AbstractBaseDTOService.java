package ru.t1.aayakovlev.tm.service.dto.impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.AbstractModelDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.model.HasCreated;
import ru.t1.aayakovlev.tm.model.HasName;
import ru.t1.aayakovlev.tm.model.HasStatus;
import ru.t1.aayakovlev.tm.repository.dto.BaseDTORepository;
import ru.t1.aayakovlev.tm.service.dto.BaseDTOService;

import java.util.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractBaseDTOService<E extends AbstractModelDTO, R extends BaseDTORepository<E>>
        implements BaseDTOService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract BaseDTORepository<E> getRepository();

    @NotNull
    protected Sort getSort(@NotNull Comparator<E> comparator) {
        @NotNull Sort sort = Sort.by(Sort.DEFAULT_DIRECTION, "id");
        if (comparator instanceof HasCreated) sort = Sort.by(Sort.DEFAULT_DIRECTION, "created");
        if (comparator instanceof HasName) sort = Sort.by(Sort.DEFAULT_DIRECTION, "name");
        if (comparator instanceof HasStatus) sort = Sort.by(Sort.DEFAULT_DIRECTION, "status");
        return sort;
    }

    @NotNull
    @Override
    @Transactional
    public E save(@Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        getRepository().save(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<E> add(@Nullable final Collection<E> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<E> resultEntities = new ArrayList<>();
        for (@NotNull final E entity : models) {
            @NotNull final E resultEntity = getRepository().save(entity);
            resultEntities.add(resultEntity);
        }
        return resultEntities;
    }

    @Override
    @Transactional
    public void clear() throws AbstractException {
        getRepository().deleteAll();
    }

    @Override
    public long count() throws AbstractException {
        return getRepository().count();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsById(id);
    }

    @NotNull
    @Override
    public List<E> findAll() throws AbstractException {
        return getRepository().findAll();
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Comparator<E> comparator) throws AbstractException {
        if (comparator == null) return findAll();
        @NotNull final Sort sort = getSort(comparator);
        return getRepository().findAll(sort);
    }

    @NotNull
    @Override
    public E findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Optional<E> modelOptional = getRepository().findById(id);
        if (!modelOptional.isPresent()) throw new EntityNotFoundException();
        return modelOptional.get();
    }

    @Override
    @Transactional
    public void remove(@Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        removeById(model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        getRepository().deleteById(id);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<E> set(@Nullable final Collection<E> models) throws AbstractException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        clear();
        return add(models);
    }

    @NotNull
    @Override
    @Transactional
    public E update(@Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        return getRepository().save(model);
    }

}
