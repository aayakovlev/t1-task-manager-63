package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("One"));
        add(new Task("Two"));
        add(new Task("Three"));
        add(new Task("Four"));
    }

    public void add(final Task project) {
        tasks.put(project.getId(), project);
    }

    public void create() {
        add(new Task("New task " + System.currentTimeMillis()));
    }

    public void deleteById(final String id) {
        tasks.remove(id);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(final String id) {
        return tasks.get(id);
    }

    public void save(Task project) {
        tasks.put(project.getId(), project);
    }

}
