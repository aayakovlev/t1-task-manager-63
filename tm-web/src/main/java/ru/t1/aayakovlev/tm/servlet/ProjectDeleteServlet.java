package ru.t1.aayakovlev.tm.servlet;

import ru.t1.aayakovlev.tm.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/projects/delete/*")
public final class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp) throws IOException {
        final String id = req.getParameter("id");
        ProjectRepository.getInstance().deleteById(id);
        resp.sendRedirect("/projects");
    }

}
