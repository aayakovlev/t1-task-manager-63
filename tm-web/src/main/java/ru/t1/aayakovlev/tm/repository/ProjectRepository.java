package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("One"));
        add(new Project("Two"));
        add(new Project("Three"));
        add(new Project("Four"));
    }

    public void add(final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new Project("New project " + System.currentTimeMillis()));
    }

    public void deleteById(final String id) {
        projects.remove(id);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void save(Project project) {
        projects.put(project.getId(), project);
    }

}
