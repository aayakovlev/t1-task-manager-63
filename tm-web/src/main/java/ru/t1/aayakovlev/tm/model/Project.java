package ru.t1.aayakovlev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.aayakovlev.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Project {

    private String id = UUID.randomUUID().toString();

    private String name;

    private Date created = new Date();

    private String description;

    private Status status = Status.NOT_STARTED;

    public Project(final String name) {
        this.name = name;
    }

}
