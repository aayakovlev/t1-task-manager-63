package ru.t1.aayakovlev.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/projects/edit/*")
public final class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Project project = ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final Status status = Status.toStatus(statusValue);
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        final String createdValue = req.getParameter("created");

        final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setStatus(status);

        if (!createdValue.isEmpty()) project.setCreated(formatter.parse(createdValue));
        else project.setCreated(null);

        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}
