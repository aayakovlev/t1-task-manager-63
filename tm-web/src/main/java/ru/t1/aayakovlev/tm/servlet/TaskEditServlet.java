package ru.t1.aayakovlev.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/tasks/edit/*")
public final class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String projectId = req.getParameter("projectId");
        final String statusValue = req.getParameter("status");
        final Status status = Status.toStatus(statusValue);
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        final String createdValue = req.getParameter("created");

        final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setProjectId(projectId);
        task.setStatus(status);

        if (!createdValue.isEmpty()) task.setCreated(formatter.parse(createdValue));
        else task.setCreated(null);

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
