package ru.t1.aayakovlev.tm.servlet;

import ru.t1.aayakovlev.tm.repository.TaskRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/tasks/create/*")
public final class TaskCreateServlet extends HttpServlet {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws IOException {
        TaskRepository.getInstance().create();
        resp.sendRedirect("/tasks");
    }

}
