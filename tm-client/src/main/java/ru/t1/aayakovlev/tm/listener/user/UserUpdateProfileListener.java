package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.UserUpdateProfileRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Update user profile.";

    @NotNull
    public static final String NAME = "user-update-profile";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.print("Enter first name: ");
        @NotNull final String firstName = nextLine();
        System.out.print("Enter last name: ");
        @NotNull final String lastName = nextLine();
        System.out.print("Enter middle name: ");
        @NotNull final String middleName = nextLine();

        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);

        userEndpoint.update(request);
    }

}
