package ru.t1.aayakovlev.tm.listener.domain;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.endpoint.DomainEndpoint;
import ru.t1.aayakovlev.tm.listener.AbstractListener;

@Component
@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    protected DomainEndpoint domainEndpoint;

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

}
