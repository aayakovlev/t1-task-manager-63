package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.UserChangePasswordRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Change user's password.";

    @NotNull
    public static final String NAME = "user-change-password";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userChangePasswordListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();
        System.out.print("Enter new password: ");
        @NotNull final String password = nextLine();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setId(id);
        request.setPassword(password);

        userEndpoint.changePassword(request);
    }

}
